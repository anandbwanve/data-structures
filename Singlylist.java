import java.util.*;

class Singlylistcode{
// node is static member class of singlylist
static class node{
    //node class fields
    private int data;
    private node next;
    //node class methods
	
    public node(){
        data=0;
        next=null;
    }
	
    public node(int val){
        data = val;
        next = null;
    }
}

    //list class fields
    private node head;
    public Singlylistcode(){
        head = null;
    }
        void display(){
            System.out.println("list: ");
            node trav = head;
            while(trav != null){
                System.out.println(trav.data);
                trav = trav.next;
            }
            System.out.println(" ");
        }

    void addfirst(int val){
        // create new node and init it
        node newNode = new node(val);
        // new node next should point to head
         newNode.next = head;
        // head should point to new node
        head = newNode;
    }

        void addlast(int val){
            // create new node and init it
            node newNode = new node(val);
            // special 1: if list is empty, make new node as first node of list
            if(head == null){
                head = newNode;
            }
            //genral: add node at the end
                    else{
                    // traverse till last node
                    node trav = head;
                    while(trav.next != null)
                    trav = trav.next;
                    // add new node into next of last node
                    trav.next = newNode; 
                    } 
        }

        void addAtPos(int val, int pos){
            //spl case 1: if list empty, add node at the start
            //spl case 2: if pos<=1, add node at the start
            if(head == null || pos <= 1)
            addfirst(val); 
            else{
            // allocate and init new node
            node newNode = new node(val);
            // traverse till pos -1(trav)
            node trav = head;
            for(int i=1; i<pos-1; i++)
            trav = trav.next;
            // newnode's next should point to trav's next
            newNode.next = trav.next;
            // newnode's next should pointer to new node
            trav.next = newNode;
         }
        }

        void delfirst(){
            // spl 1: if list is emty throw exception
            if(head == null)
            throw new RuntimeException("list is empty");
            // general: make head point to next node
            head = head.next;
            // note: the old dirst node will be garbage collected
        }

        void delAll(){
            head = null;
        }

        void delAtPos(int pos){
            // spl 1: if pos = 1, delete first node
            if(pos ==1)
            delfirst();
            // spl 2: if list is empty or pos<1, then throw exception
            if(head == null || pos < 1)
            throw new RuntimeException("list is empty or invalid position");
            // take temp pointer traversing behind trav
            node temp = null , trav = head;
            // traverse till pos (trav)
            for(int i=1; i< pos; i++){
                // spl 3: if pos is beyond list length, throw exception
                if(trav == null)
                throw new RuntimeException("invalid position");
                temp = trav;
                trav = trav.next;
            }
            // trav is node to be delete & temp is node before that
            temp.next = trav.next;
            // trave node will be garbage collected
        }

        void delLast(){
            // spl 1: if list empty , throw exception
            if(head == null)
            throw new RuntimeException("list is empty");
            // sp 2 : if list has single node,  make head null
            if(head.next == null)
            head = null;
            //general: delete last node
            node  temp =null, trav=head;
            // trav till last node and run temp behind it
            while(trav.next != null){
                temp = trav;
                trav = trav.next;
            }
            // when last node deleted, 2nd last node (temp) should be null
            temp.next = null;
        }
}

public class Singlylist {
    public static void main(String[] args) {
        int choice, val, pos;
        Singlylistcode list = new Singlylistcode();
        Scanner sc = new Scanner(System.in);
        do{
            System.out.println("\n \n 1.display \n 2.add first \n 3.add last \n 4.add at pos \n 5.del first \n 6.del last \n 7.del at \n 8.del all");
            choice = sc.nextInt();
            switch(choice){
                case 1: //display
                list.display();
                break;

                case 2: //add first
                System.out.println("enter the element: ");
                val = sc.nextInt();
                list.addfirst(val);
                break;

                case 3: //add last
                System.out.println("enter the element: ");
                val = sc.nextInt();
                list.addlast(val);
                break;
                case 4: //add at pos
                System.out.println("enter new element: ");
                val = sc.nextInt();
                System.out.println("enter element position");
                pos = sc.nextInt();
                list.addAtPos(val, pos);
                break;
                case 5: //del first
                try {
                    list.delfirst();
                } catch (Exception e) {
                   System.out.println(e.getMessage());
                }
                break;
                case 6: //del last
                try {
                    list.delLast();
                } catch (Exception e) {
                   System.out.println(e.getMessage());
                }
                break;
                
                case 7: //del at pos
                System.out.println("enter the position to be deleted: ");
                pos = sc.nextInt();
                list.delAtPos(pos);
                break;

                case 8: // del all
                list.delAll();
                break;
            }
        }
        while(choice !=0);
    }
}
  