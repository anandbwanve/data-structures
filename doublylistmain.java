import java.util.*;

class doublylist{

    //node class
    static class node{

        private int data;
        private node next;
        private node prev;

        public node(){
            data = 0;
            next = null;
            prev = null;
        }

        public node(int val){
            data = val;
            next = null;
            prev = null;
        }
    }

    // list class fields
    private node head;

    // list class methods
    public doublylist(){
        head = null;
    }

    public void displayForward(){
        System.out.println("fwd list:");
        node trav = head;
        while (trav != null){
            System.out.println(trav.data);
            trav = trav.next;  
        }
    }

    public void displayReverse(){
        System.out.println("rev list: ");
        if(head == null)
        return;
        // trav till last node
        node trav = head;
        while(trav.next != null)
        trav = trav.next;
        while(trav != null){
        System.out.println(trav.data);
        trav = trav.prev;
        }
    }

    public void addlast(int val){
        node newNode = new node(val);

        if(head == null){
            head = newNode;
        }

        else {
            node trav = head;
            while(trav.next != null)
            trav = trav.next;

            trav.next = newNode;

            newNode.prev = trav;
        }
    }
    public boolean isEmpty(){
        return head == null;
    }
    void addfirst(int val){
       
        node newNode = new node(val);
        // spl1 1 :  if list is empty
        if(isEmpty()){
            head = newNode;
        }
        else{
        newNode.next = head;
        head.prev = newNode;
        head = newNode;
        }
    }  
    
    void addAtPos(int val, int pos){
            if(head == null || pos <=1)
            addfirst(val);
        
            else{
        node newNode = new node(val);

        node temp, trav = head;
            for (int i = 1; i < pos -1; i++) {
                
                if(trav.next == null) //if pos > length, add node at the end
                break;
              trav = trav.next;
            }
            temp = trav.next;

            newNode.next = temp;

            newNode.prev = trav;

            trav.next = newNode;

            if(temp != null)    // spl3: if adding at end, nextline is not required
            temp.prev = newNode;
        }
    }

    void delfirst(){
        //spl1
        if(head == null)
        throw new RuntimeException("list is empty");
        // spl2: single node, make head null
        if(head.next == null)
        head = null;
        else{
        head = head.next;

        head.prev = null;
        }

    }

    void delAtPos(int pos){
        if(pos == 1)
        delfirst();

        if(head == null || pos < 1)
        throw new RuntimeException("list empty or invalid pos");

        node trav = head;
        for(int i=1; i<pos; i++){
            if(trav == null) 
            throw new RuntimeException("list empty or invalid pos");
            trav = trav.next;
        }
        
        trav.prev.next = trav.next;
        if(trav.next != null)
        trav.next.prev = trav.prev;

    }
}

 class doublylistmain {
    public static void main(String[] args) {
        int choice, val, pos;
       doublylist list = new doublylist();
        Scanner sc = new Scanner(System.in);
        do{
            System.out.println("\n \n 1.display \n 2.add first \n 3.add last \n 4.add at pos \n 5.del first \n 6.del last \n 7.del at \n 8.del all");
            choice = sc.nextInt();
            switch(choice){
                case 1: //display
                list.displayForward();
                list.displayReverse();
                break;

                case 2: //add first
                System.out.println("enter the element: ");
                val = sc.nextInt();
               list.addfirst(val);
                break;

                case 3: //add last
                System.out.println("enter the element: ");
                val = sc.nextInt();
                list.addlast(val);
                break;

                case 4: //add at pos
                System.out.println("enter new element: ");
                val = sc.nextInt();
                System.out.println("enter element position");
                pos = sc.nextInt();
               list.addAtPos(val, pos);
                break;

                case 5: //del first
                try {
               list.delfirst();
                } catch (Exception e) {
                   System.out.println(e.getMessage());
                }
                break;

                case 6: //del last
                try {
               //     list.delLast();
                } catch (Exception e) {
                   System.out.println(e.getMessage());
                }
                break;
                
                case 7: //del at pos
                System.out.println("enter the position to be deleted: ");
                pos = sc.nextInt();
               list.delAtPos(pos);
                break;

                case 8: // del all
              //  list.delAll();
                break;
            }
        }
        while(choice !=0);
    }
    
}

