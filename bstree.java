import java.util.*;
class binSearchTree{
    static class node{
        private int data;
        private node left, right;

        public node(){
            data = 0;
            left = null;
            right = null;
        }

        public node(int val){
            data = val;
            left = null;
            right = null;
        }
    }

    private node root;

    public binSearchTree(){
        root =null;
    }

    void add(int val){
        node newNode = new node(val);
        if(root == null)
        root = newNode;
        else{
            node trav = root;
            while(true){
                if(val < trav.data){
                    if(trav.left != null)
                    trav = trav.left;
                    else{
                        trav.left = newNode;
                        break;
                    }
                }
                else{
                    if(trav.right != null)
                    trav = trav.right;
                    else{
                        trav.right = newNode;
                        break;
                    }
                }
            } 
        }
    }

    void preorder(node trav){
        if(trav == null)
        return;
        System.out.print(trav.data + " ");
        preorder(trav.left);
        preorder(trav.right);
    }

    void preorder(){
        preorder(root);
        System.out.println();
    }

    void inorder(node trav){
        if(trav == null)
        return;
        
        inorder(trav.left);
        System.out.print(trav.data + " ");
        inorder(trav.right);
    }

    void inorder(){
        inorder(root);
        System.out.println();
    }

    void postorder(node trav){
        if(trav == null)
        return;
        
        postorder(trav.left);
        postorder(trav.right);
        System.out.print(trav.data + " ");

    }

    void postorder(){
        postorder(root);
        System.out.println();
    }

  node binSearch(int key){
    node trav = root;
    while (trav != null){
    if(key == trav.data)
    return trav;
    if(key < trav.data)
    trav = trav.left;
    if(key > trav.data)
    trav = trav.right;
    }
    return null;
  }

  node[] binSearchwithParent(int key){
    node trav = root, parent = null;
    while (trav != null){
        if(key == trav.data)
            return new node[]{trav,parent};
        parent = trav;
        if(key < trav.data)
            trav = trav.left;
        else //if(key > trav.data)
            trav = trav.right;
    }
    return new node[] {null, null};
  }


    public void delete(int val){
        node trav, parent;
        node[] arr = binSearchwithParent(val);
        trav = arr[0];
        parent = arr[1];

        if(trav == null)
        throw new RuntimeException("node not found");

        if(trav.left != null && trav.right != null){
            parent = trav;
            node succ = trav.right;
            while(succ.left != null){
                parent = succ;
                succ = succ.left;
            }
            trav.data = succ.data; //override

            trav = succ; // succ deleted
        }

        if(trav.left == null){   // if node has right chikd
        if(trav == root)
             root = trav.right;

        else if(trav == parent.left)
             parent.left = trav.right;

        else
            parent.right = trav.right;
        } 

        else if (trav.right == null){ //if it dosebnt have right child
            if(trav == root)
            root = trav.left;
            else if(trav == parent.left)
            parent.left = trav.left;
            else
            parent.right = trav.left;
            } 
        }
    }






class bstree{
    public static void main(String args[]){
        Scanner sc = new Scanner(System.in);
        binSearchTree t = new binSearchTree();
        t.add(50);
       // t.add(10);

        t.add(30);

        t.add(70);

        t.add(20);

        t.add(90);

        t.add(10);

        t.add(70);

        t.preorder();

        t.inorder();

        t.postorder();
        
        t.binSearch(50);
        
     /*  binSearchTree.node resultNode = t.binSearch(90);
        if(resultNode != null){
            System.out.println("key found");
        }
        else{
            System.out.println("key not found");
        }
        */

     
     System.out.println("enter ele to del");
     int val = sc.nextInt();
     t.delete(val);

     t.inorder();


    }
}   