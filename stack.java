import java.util.*;
class stackoperations{
    private int arr[];
    private int top;

    public stackoperations(int size){
        arr = new int[size];
        top = -1;
    }

    public boolean isEmpty(){
        return top == -1;
    }

    public boolean isFull(){
        return top == arr.length-1;
    }

    void push(int val){
        if(isFull()){
            throw new RuntimeException("stack is full");
        }
        top++;
        arr[top] = val;
        
    }

    
    void pop(){
        if(isEmpty())
             throw new RuntimeException("stack is empty");
        top--;
    }

    int peek(){
        return arr[top];
    }

}


    class stack{
        public static void main(String args[]){
            Scanner sc  = new Scanner(System.in);
        stackoperations s = new stackoperations(2);
            int choice;

        do{
            System.out.println("\n0. exist \n1. push \n2. pop \n3. peek");
            choice  = sc.nextInt();

        switch(choice){
            case 1 : 
            try{
            System.out.println("\nEnter ele: ");
            s.push(sc.nextInt());
            } catch (Exception e){
                System.out.println(e.getMessage());
            }
            break;

            case 2 : 
            try{
                System.out.println("\nPopped: " + s.peek());
                s.pop();
                } catch (Exception e){
                    System.out.println(e.getMessage());
                }
            break;

            case 3 : 
            try{
                System.out.println("\nPeek: " + s.peek());
                } catch (Exception e){
                    System.out.println(e.getMessage());
                }
            break;
        }
        }
        while(choice != 0);
        sc.close();

        }
    }