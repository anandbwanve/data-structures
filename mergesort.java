public class mergesort {

    static void conquer(int arr[], int si ,int mid , int ei){
        int merged[] = new int[ei - si + 1];

        int i1 = si;
        int i2 = mid + 1;
        int x = 0;
        while(i1 <= mid && i2 <= ei){
            if(arr[i1] <= arr[i2]){
                merged[x] = arr[i1];
                x++; i1++;
            }   
            
            else{
               // if(arr[i2] <= arr[i1])
                    merged[x] = arr[i2];
                x++; i2++;
            }
        }

        while(i1 <= mid){
            merged[x] = arr[i1];
            x++; 
            i1++;
        }

        while(i2 <= ei){
            merged[x] = arr[i2];
            x++;  i2++;
    }

    for(int i = 0, j=si; i< merged.length; i++, j++){
        arr[j] = merged[i];
    }
}


    static void divide(int arr[], int si, int ei){
        if(si>=ei)
            return;

   // mid = (si + ei) /2; 
    int mid =  si + (ei -si)/2;
    divide(arr, si ,mid);
    divide(arr, mid+1, ei);
    conquer(arr, si ,mid ,ei);
}    

public static void main(String[] args) {
        int arr[]= {6,3,9,5,2,8};
        int n = arr.length;
        divide(arr, 0, n-1);

       for(int i=0; i<n; i++){
        System.out.print(arr[i] + " ");
       }
       System.out.println();
    }
}


//AI EXPLAINED
/* 
public class mergesort {

    static void conquer(int arr[], int si ,int mid , int ei){
        // This function merges two sorted subarrays of arr[].
        // The first subarray is arr[si..mid]
        // The second subarray is arr[mid+1..ei]

        // Create a temp array to store the merged subarry
        int merged[] = new int[ei - si + 1];

        // Indexes of first and second subarrays
        int i1 = si;
        int i2 = mid + 1;
        int x = 0;

        // Iterate over both subarrays and copy the smaller element to merged[]
        while(i1 <= mid && i2 <= ei){
            if(arr[i1] <= arr[i2]){
                // Copy the element from the first subarray
                merged[x] = arr[i1];
                x++; i1++;
            }   
            
            else{
                // Copy the element from the second subarray
                merged[x] = arr[i2];
                x++; i2++;
            }
        }

        // Copy the remaining elements from the first subarray
        while(i1 <= mid){
            merged[x] = arr[i1];
            x++; 
            i1++;
        }

        // Copy the remaining elements from the second subarray
        while(i2 <= ei){
            merged[x] = arr[i2];
            x++;  i2++;
        }

        // Copy the merged elements from temp[] to arr[]
        for(int i = 0, j=si; i< merged.length; i++, j++){
            arr[j] = merged[i];
        }
    }


    static void divide(int arr[], int si, int ei){
        // This function divides the array arr[] into two subarrays arr[si..mid] and arr[mid+1..ei]
        // The function then recursively sorts the two subarrays

        // Base case: If the array has only one element, do nothing
        if(si>=ei)
            return;

        // Find the middle point of the array
        int mid =  si + (ei -si)/2;

        // Divide the array into two subarrays
        divide(arr, si ,mid);
        divide(arr, mid+1, ei);

        // Merge the two sorted subarrays
        conquer(arr, si ,mid ,ei);
    }    

public static void main(String[] args) {
        // This is the main function. It takes an array of integers as input and sorts it using merge sort

        // Create an array of integers
        int arr[]= {6,3,9,5,2,8};

        // Get the number of elements in the array
        int n = arr.length;

        // Sort the array using merge sort
        divide(arr, 0, n-1);

        // Print the sorted array
        for(int i=0; i<n; i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
*/