import java.util.Scanner;

// This class represents a node in a linked list.
class Node {
    // The data stored in the node.
    int data;
    // The next node in the linked list.
    Node next;

    // Constructor for the Node class.
    Node(int data) {
        this.data = data;
        this.next = null;
    }
}

// This class represents a queue.
class Queue {
    // The front of the queue.
    Node front;
    // The rear of the queue.
    Node rear;

    // Constructor for the Queue class.
    Queue() {
        this.front = this.rear = null;
    }

    // Inserts a new element into the queue.
    void insert(int data) {
        // Create a new node to store the data.
        Node newNode = new Node(data);

        // If the queue is empty, set the front and rear of the queue to the new node.
        if (this.rear == null) {
            this.front = this.rear = newNode;
            return;
        }

        // Set the next node of the rear of the queue to the new node.
        this.rear.next = newNode;
        // Set the rear of the queue to the new node.
        this.rear = newNode;
    }

    // Deletes the element at the front of the queue.
    void delete() {
        // If the queue is empty, do nothing.
        if (this.front == null) {
            System.out.println("Queue is empty");
            return;
        }

        // Store the front of the queue in a temporary variable.
        Node temp = this.front;
        // Set the front of the queue to the next node.
        this.front = this.front.next;

        // If the front of the queue is now null, set the rear of the queue to null.
        if (this.front == null)
            this.rear = null;

        // Free the memory used by the temporary variable.
        temp = null;
    }

    // Displays the contents of the queue.
    void display() {
        // If the queue is empty, do nothing.
        if (this.front == null) {
            System.out.println("NULL");
            return;
        }

        // Create a temporary node to traverse the queue.
        Node temp = this.front;

        // Print the data of each node, followed by an arrow.
        while (temp != null) {
             System.out.print("->" + temp.data);
            temp = temp.next;
        }
        // Print a newline character.
        System.out.println();
    }
}

public class Main {
    public static void main(String[] args) {
        // Create a scanner object to read input from the user.
        Scanner sc = new Scanner(System.in);
        // Create a queue object.
        Queue q = new Queue();
        // Declare variables to store the user's choice and data.
        int choice, data;

        // Do this loop until the user chooses to quit.
        do {
            // Get the user's choice.
            choice = sc.nextInt();

            // Switch on the user's choice.
            switch (choice) {
                case 1:
                    // Get the data to insert into the queue.
                    data = sc.nextInt();
                    // Insert the data into the queue.
                    q.insert(data);
                    break;
                case 2:
                    // Delete the element at the front of the queue.
                    q.delete();
                    break;
                case 3:
                    // Display the contents of the queue.
                    q.display();
                    break;
                case 4:
                    // Quit the program.
                    break;
                default:
                    // Print an error message if the user enters an invalid choice.
                    System.out.println("Invalid choice!");
            }
        } while (choice != 4);
    }
}
